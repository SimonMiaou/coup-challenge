require 'dcf'

class PackageIndexerJob < ApplicationJob
  queue_as :default

  def perform(payload)
    package_list = Dcf.parse(payload)
    package_list.each do |entry|
      Package.find_or_create_by(name: entry['Package'], version: entry['Version'])
    end
  end
end
