require 'open-uri'

class PackageListFetcherJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    URI.open("#{ENV['CRAN_SERVER']}PACKAGES") do |f|
      payloads = f.read.split("\n\n")
      payloads.each do |payload|
        PackageIndexerJob.perform_later(payload.strip)
      end
    end
  end
end
