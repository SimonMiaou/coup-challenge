require 'test_helper'

class PackageListFetcherJobTest < ActiveJob::TestCase
  test 'split the CRAN server\'s PACKAGES file and spawn jobs for each package' do
    stub_request(:get, 'https://cran.r-project.org/src/contrib/PACKAGES')
      .to_return(status: 200, body: File.read(Rails.root.join('test', 'fixtures', 'files', 'PACKAGES')), headers: {})

    mock(PackageIndexerJob).perform_later("Package: A3\n" \
                                          "Version: 1.0.0\n" \
                                          "Depends: R (>= 2.15.0), xtable, pbapply\n" \
                                          "Suggests: randomForest, e1071\n" \
                                          "License: GPL (>= 2)\n" \
                                          'NeedsCompilation: no')
    mock(PackageIndexerJob).perform_later("Package: aaSEA\n" \
                                          "Version: 1.0.0\n" \
                                          "Depends: R(>= 3.4.0)\n" \
                                          "Imports: DT(>= 0.4), networkD3(>= 0.4), shiny(>= 1.0.5),\n" \
                                          "        shinydashboard(>= 0.7.0), magrittr(>= 1.5), Bios2cor(>= 1.2),\n" \
                                          "        seqinr(>= 3.4-5), plotly(>= 4.7.1), Hmisc(>= 4.1-1)\n" \
                                          "Suggests: knitr, rmarkdown\n" \
                                          "License: GPL-3\n" \
                                          'NeedsCompilation: no')
    mock(PackageIndexerJob).perform_later("Package: abbyyR\n" \
                                          "Version: 0.5.5\n" \
                                          "Depends: R (>= 3.2.0)\n" \
                                          "Imports: httr, XML, curl, readr, plyr, progress\n" \
                                          "Suggests: testthat, rmarkdown, knitr (>= 1.11), lintr\n" \
                                          "License: MIT + file LICENSE\n" \
                                          'NeedsCompilation: no')
    mock(PackageIndexerJob).perform_later("Package: abc\n" \
                                          "Version: 2.1\n" \
                                          "Depends: R (>= 2.10), abc.data, nnet, quantreg, MASS, locfit\n" \
                                          "License: GPL (>= 3)\n" \
                                          'NeedsCompilation: no')
    mock(PackageIndexerJob).perform_later("Package: abc.data\n" \
                                          "Version: 1.0\n" \
                                          "Depends: R (>= 2.10)\n" \
                                          "License: GPL (>= 3)\n" \
                                          'NeedsCompilation: no')

    PackageListFetcherJob.perform_now
  end
end
