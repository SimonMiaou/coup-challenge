require 'test_helper'

class PackageIndexerJobTest < ActiveJob::TestCase
  test 'create a package based on the CRAN PACKAGES payload for one package' do
    PackageIndexerJob.perform_now("Package: A3\n" \
                                           "Version: 1.0.0\n" \
                                           "Depends: R (>= 2.15.0), xtable, pbapply\n" \
                                           "Suggests: randomForest, e1071\n" \
                                           "License: GPL (>= 2)\n" \
                                           'NeedsCompilation: no')

    assert_equal([['A3', '1.0.0']],
                 Package.order(:name, :version).map { |package| [package.name, package.version] })
  end

  test 'avoid duplicating packages' do
    PackageIndexerJob.perform_now("Package: A3\n" \
                                           "Version: 1.0.0\n" \
                                           "Depends: R (>= 2.15.0), xtable, pbapply\n" \
                                           "Suggests: randomForest, e1071\n" \
                                           "License: GPL (>= 2)\n" \
                                           'NeedsCompilation: no')

    PackageIndexerJob.perform_now("Package: A3\n" \
                                        "Version: 1.0.0\n" \
                                        "Depends: R (>= 2.15.0), xtable, pbapply\n" \
                                        "Suggests: randomForest, e1071\n" \
                                        "License: GPL (>= 2)\n" \
                                        'NeedsCompilation: no')

    assert_equal([['A3', '1.0.0']],
                 Package.order(:name, :version).map { |package| [package.name, package.version] })
  end

  test 'keep track of all versions of a package' do
    PackageIndexerJob.perform_now("Package: A3\n" \
                                       "Version: 1.0.0\n" \
                                       "Depends: R (>= 2.15.0), xtable, pbapply\n" \
                                       "Suggests: randomForest, e1071\n" \
                                       "License: GPL (>= 2)\n" \
                                       'NeedsCompilation: no')

    PackageIndexerJob.perform_now("Package: A3\n" \
                                        "Version: 2.0.0\n" \
                                        "Depends: R (>= 2.15.0), xtable, pbapply\n" \
                                        "Suggests: randomForest, e1071\n" \
                                        "License: GPL (>= 2)\n" \
                                        'NeedsCompilation: no')

    assert_equal([['A3', '1.0.0'], ['A3', '2.0.0']],
                 Package.order(:name, :version).map { |package| [package.name, package.version] })
  end
end
